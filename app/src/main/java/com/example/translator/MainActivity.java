package com.example.translator;

import android.inputmethodservice.Keyboard;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private TranslatorViewModel translatorViewModel;
    private RequestQueue requestQueue;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    return true;
                case R.id.navigation_dashboard:
                    return true;
                case R.id.navigation_notifications:
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        requestQueue = MyRequestQueue.getQueue(this);

        translatorViewModel = ViewModelProviders.of(this).get(TranslatorViewModel.class);

        final TextView labelInput = findViewById(R.id.label_input);
        translatorViewModel.getUserInput().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                labelInput.setText(s);
            }
        });

        final TextView labelOutput = findViewById(R.id.label_output);
        translatorViewModel.getTranslationResult().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                labelOutput.setText(s);
            }
        });

        Button buttonTranslate = findViewById(R.id.button_translate);
        buttonTranslate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("MYTRANSLATOR", "Clicked on translate");
                EditText input = findViewById(R.id.text_input);
                Log.v("MYTRANSLATOR", "User input: " + input);

                translatorViewModel.setUserInput(input.getText().toString());

                //API CALL
                String url ="https://api.mymemory.translated.net/get?q="+input.getText().toString()+"&langpair=en|cs";

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject responseData = response.getJSONObject("responseData");
                            String translatedText = responseData.getString("translatedText");

                            translatorViewModel.setTranslationResult(translatedText);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse (VolleyError error){
                            labelOutput.setText(error.toString());
                        }
                    }
                );

                requestQueue.add(jsonObjectRequest);

                // hiding keyboard
                input.onEditorAction(EditorInfo.IME_ACTION_DONE);
            }
        });
    }

}
