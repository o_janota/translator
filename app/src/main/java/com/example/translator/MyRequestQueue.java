package com.example.translator;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class MyRequestQueue {
    private static RequestQueue queue = null;

    public static RequestQueue getQueue(Context context) {
        if (queue == null) {
            return Volley.newRequestQueue(context);
        }
        return queue;
    }
}
