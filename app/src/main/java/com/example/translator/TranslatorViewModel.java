package com.example.translator;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TranslatorViewModel extends ViewModel {

    private MutableLiveData<String> userInput, translationResult;

    public TranslatorViewModel() {
        userInput = new MutableLiveData<>();
        translationResult = new MutableLiveData<>();
    }


    public LiveData<String> getUserInput() {
        return userInput;
    }

    public void setUserInput(String text) {
        userInput.setValue(text);
    }

    public LiveData<String> getTranslationResult() {
        return translationResult;
    }

    public void setTranslationResult(String text) {
        translationResult.setValue(text);
    }
}
